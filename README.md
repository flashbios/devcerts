# Openssl root CA and server certs generation #

 * You need to play sometimes with local fake domains with the https protocol
 * You're tired of wasted time confirming the `I accept the risk` for self-signed certificates
 * You're tired of wasted time bypassing ssl checks and doing some useless extra conf on your tests 
 * This is for you

The goal of these scripts is pretty simple :

 - Create a private key and a certificate for our own root CA (ca-cert.sh)
 - Create a csr and a signed certificate for your webserver using this root CA (server-cert.sh)

## Requirements
openssl

## Create a private key and a certificate for our own root CA
```shell
$ ./ca-cert.sh
---------------------------------------------------------
Please provide a name for your organization root CA 
[default:MyAwesomeOrgCA]: FlashbiosNetworksCA
---------------------------------------------------------
Please provide a country code
[default:FR]: 
---------------------------------------------------------
Please provide a state or province 
[default:PACA]: 
---------------------------------------------------------
Please provide a locality or city 
[default:'Roquefort-la-bédoule']: 
---------------------------------------------------------
Please provide a name for your organization 
[default:MyAwesomeOrg]: FlashbiosNetworks  
---------------------------------------------------------
Please provide a validity period in days (1826 = 5years) 
[default:1826]: 
---------------------------------------------------------
Please enter 3 times the same pass phrase (4+ chars) 
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
Enter pass phrase for root-ca/FlashbiosNetworksCA/FlashbiosNetworksCA.key:
---------------------------------------------------------
Your key and root CA have been generated in root-ca/FlashbiosNetworksCA : 
total 12K
drwxr-xr-x 1 flashbios flashbios  140 28 avril 19:21 .
drwxr-xr-x 1 flashbios flashbios   38 28 avril 19:21 ..
-rw-r--r-- 1 flashbios flashbios 2,1K 28 avril 19:21 FlashbiosNetworksCA.crt
-rw------- 1 flashbios flashbios 3,4K 28 avril 19:21 FlashbiosNetworksCA.key
-rw-r--r-- 1 flashbios flashbios   81 28 avril 19:21 FlashbiosNetworksCA.subj
Remember to NEVER share the key nor the pass phrase 
---------------------------------------------------------
```

## Create a csr and a signed certificate for your webserver using this root CA
```shell
$ ./server-cert.sh FlashbiosNetworksCA test.flashbios.net
.....+...............+...............+......+.......+......+.....+.+........+.+...............+..+....+...+...+.....+.......+++++++++++++++++++++++++++++++++++++++++++++*.....+...+...+.....+......+...+.+......+++++++++++++++++++++++++++++++++++++++++++++*.....+...............+...........+...+.+...........+++++
..+.+...........+.+...+...........+...+.+......+......+++++++++++++++++++++++++++++++++++++++++++++*...+....+..+.......+...+..+.+..............+.......+...+......+..+...+....+...+...+.....+.+.....+..........+...+..+.+..............+.+...+..............+...+++++++++++++++++++++++++++++++++++++++++++++*..+........+...................+.....+....+.....+.........+...+...+....+...........+......+.........+..........................................+....+...+...............+.....+..........+...........+.......+...+..+.......+........+...+......+....+...........+.+....................+..........+..+............+...+......+..........+.....+.+...............+.....+.+........+....+...+...+..+............+..........+........+....+.....+.........+......+.........+......................+...+..+.....................................+........+...+.........+......+.........+...+....+...........+..........+.....+.........+.+.................................+..............+.+......+...........+.......+.................+.........+....+............+...........+....+...+...........+..........+......+.........+..+............+............+....+............+..+.+.........+...........+...................+...+...............+..+...+..................+......+.........+.......+.....+.........+....+.........+.....+.+....................+.......+..+.......+...+..+.........+.+...........+.......+......+..+....+.....+...+......+.......+...+......+......+..............+.+.....+....+..............+......+.+..............+....+..+............+...............+.+..+............+.+......+......+...+..+...+.........+...+.....................................+..+......+...+.+......+...+.........+..+.+...+.....+.+..........................+..........+........+.........+.+..+....+.....+...+.......+.............................+.........+.+....................+..........+.....+......+.............+..+....+..+..........+.....+....+.....+....+.....+.....................+.............+...+.....+...............+...................+...+...+.....+....+...+......+.....+......+.+........+....+...........+......+......+...............+.......+..+...+.+.....+....+...........+.+.....+...+...............+.+..+....+...+..........................+.......+.................+................+.....+.+......+...+...........+...+...+....+........+...+.......+...+...............+..+.......+..................+...+.....+....+......+........+...............+.......+........+.+...+..+....+...........+...............+.........+.......................................+.............+..+..........+......+.....+......+.+...+......+...+...............+.....+...............+......+.+.........+.................+.........+.............+...+...+......+..............+.........+................+.....+.......+.....+............+..........+...............+...+.........+.....+....+..+....+...+...........+.+.....+....+.....+.+.........+.........+............+...+........+..................................+...+......+..+...+.......+...+...........+.+...+......+.....+...+.......+..................+..+..................+.+.....+................+...+...+....................+...+.+.....+......+...+.+.....+.+...+.....................+..............+.......+............+.....+.....................+.......+........+......+.........+....+...+.........+...............+....................+....+............+...+..........................+....+..+.......+..+.+.....+....+........+.+..............+..........+...........+............+............+.............+...+...........+......+.......+.....+.............+.....+...................+.....+....+......+...+...........+....+..+.............+..+.+.................+...+++++
-----
Certificate request self-signature ok
subject=CN=FlashbiosNetworksCA, C=FR, ST=PACA, L=Roquefort-la-bÃ©doule, O=FlashbiosNetworks
Enter pass phrase for root-ca/FlashbiosNetworksCA/FlashbiosNetworksCA.key:
---------------------------------------------------------
Your key and cert have been generated in server-cert/test.flashbios.net : 
total 16K
drwxr-xr-x 1 flashbios flashbios  182 28 avril 20:21 .
drwxr-xr-x 1 flashbios flashbios   72 28 avril 20:21 ..
-rw-r--r-- 1 flashbios flashbios 2,1K 28 avril 20:21 test.flashbios.net.crt
-rw-r--r-- 1 flashbios flashbios 1,7K 28 avril 20:21 test.flashbios.net.csr
-rw------- 1 flashbios flashbios 3,2K 28 avril 20:21 test.flashbios.net.key
-rw-r--r-- 1 flashbios flashbios  208 28 avril 20:21 test.flashbios.net.v3.ext
---------------------------------------------------------
```


## Deploy the certificate
This part may differ depending on the server you are using,
here's an example inside an (already configured) apache VirtualHost conf file :
```shell
SSLCertificateFile /path/to/test.flashbios.net.crt
SSLCertificateKeyFile /path/to/test.flashbios.net.key
```


## Import this CA on your local machines/browsers
This part may differ depending on your systems and browsers, but it's easy to find some documentation.
Feel free to contribute to this README if you think it differently
e.g. on arch based distros :
```shell
sudo trust anchor /path/to/FlashbiosNetworksCA.crt
sudo update-ca-trust
```
N.B.: Some browsers may need to explicitly import the CA to work smoothly with this trick.


## Manually resolve the hostname
You will have to edit as administrator the hosts file to add a line
On *NIX systems, it's `/etc/hosts`
On Windows systems, it's `C:\Windows\System32\drivers\etc\hosts`
The IP can be 127.0.0.1 (localhost) or the IP of a server machine on your LAN using the cert
eg : 
```shell
127.0.0.1 test.flashbios.net
```


## Test it
Try to open the URL in your browser 
```https://test.flashbios.net```
It should open without any alert, with the secure green lock on the URL bar.