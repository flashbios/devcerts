#!/bin/bash

# COLORS
NC='\033[0m'
RED='\033[1;31m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'

ca_dir="root-ca"
cert_dir="server-cert"

function usage() {
    cat << EOF
  USAGE : $0 [ca_name] [fqdn]
EOF
exit
}

if [[ -z "$1" || -z "$2" ]]; then
  usage
  exit
fi
ca_name="${1}"
ca_path="${ca_dir}/${ca_name}"

if [[ ! -f "${ca_path}/${ca_name}.crt" ||  ! -f "${ca_path}/${ca_name}.subj" ]]; then
    echo -e "${RED}Missing crt or subj in ${ca_path}${NC}"
    exit 1
fi

fqdn="${2}"
subj="$(cat "${ca_path}/${ca_name}.subj")"
cert_path="${cert_dir}/${fqdn}"
mkdir -p "${cert_path}"
# create csr
openssl req -new -nodes -out "${cert_path}/${fqdn}.csr" -newkey rsa:4096 -keyout "${cert_path}/${fqdn}.key" -subj "${subj}"
# create a v3 ext file for SAN properties
cat > "${cert_path}/${fqdn}.v3.ext" << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $fqdn
EOF
# create cert
openssl x509 -req -in "${cert_path}/${fqdn}.csr" -CA "${ca_path}/${ca_name}.crt" -CAkey "${ca_path}/${ca_name}.key" -CAcreateserial -out "${cert_path}/${fqdn}.crt" -days 730 -sha256 -extfile "${cert_path}/${fqdn}.v3.ext"

if [[ -f "${cert_path}/${fqdn}.key" && -f "${cert_path}/${fqdn}.crt" ]]; then
    echo -e "${CYAN}---------------------------------------------------------${NC}"
    echo -e "${GREEN}Your key and cert have been generated in ${cert_path} : ${NC}"
    ls -lha "${cert_path}"
    echo -e "${CYAN}---------------------------------------------------------${NC}"
fi