#!/bin/bash

# COLORS
NC='\033[0m'
RED='\033[1;31m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'

ca_dir="root-ca"

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${YELLOW}Please provide a name for your organization root CA ${NC}"
read -r -p "[default:MyAwesomeOrgCA]: " req_ca_name
ca_name=${req_ca_name:-MyAwesomeOrgCA}

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${CYAN}Please provide a country code${NC}"
read -r -p "[default:FR]: " req_country
ca_country=${req_country:-FR}

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${CYAN}Please provide a state or province ${NC}"
read -r -p "[default:PACA]: " req_state
ca_state=${req_state:-PACA}

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${CYAN}Please provide a locality or city ${NC}"
read -r -p "[default:'Roquefort-la-bédoule']: " req_locality
ca_locality=${req_locality:-'Roquefort-la-bédoule'}

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${CYAN}Please provide a name for your organization ${NC}"
read -r -p "[default:MyAwesomeOrg]: " req_org_name
ca_org_name=${req_org_name:-MyAwesomeOrg}

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${CYAN}Please provide a validity period in days (1826 = 5years) ${NC}"
read -r -p "[default:1826]: " req_validity
ca_validity=${req_validity:-1826}

ca_subj="/CN=${ca_name}/C=${ca_country}/ST=${ca_state}/L=${ca_locality}/O=${ca_org_name}"

ca_path="${ca_dir}/${ca_name}"
mkdir -p "${ca_path}"

# persist these infos for later server-cert gen
echo "${ca_subj}" > "${ca_path}/${ca_name}.subj"

echo -e "${CYAN}---------------------------------------------------------${NC}"
echo -e "${CYAN}Please enter 3 times the same pass phrase (4+ chars) ${NC}"
# generate aes encrypted private key
openssl genrsa -aes256 -out "${ca_path}/${ca_name}.key" 4096

# gen certificate
openssl req -x509 -new -nodes -key "${ca_path}/${ca_name}.key" -sha256 -days "${ca_validity}" -out "${ca_path}/${ca_name}.crt" -subj "${ca_subj}"
if [[ -f "${ca_path}/${ca_name}.key" && -f "${ca_path}/${ca_name}.crt" ]]; then
    echo -e "${CYAN}---------------------------------------------------------${NC}"
    echo -e "${GREEN}Your key and root CA have been generated in ${ca_path} : ${NC}"
    ls -lha "${ca_path}"
    echo -e "${RED}Remember to NEVER share the key nor the pass phrase ${NC}"
    echo -e "${CYAN}---------------------------------------------------------${NC}"
fi